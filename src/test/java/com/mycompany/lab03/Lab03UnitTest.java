package com.mycompany.lab03;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Gift
 */
public class Lab03UnitTest {
    
    public Lab03UnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWinXR1_output_true(){
        char [][]table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char player = 'X';
        boolean result = Lab03.checkWin_RowCol(table, player);
        assertEquals(true, result);
        
    }
    
    @Test
    public void testCheckWinOR1_output_true(){
        char [][]table = {{'O','O','O'},{'-','X','-'},{'X','-','O'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinXR1_output_false(){
        char [][]table = {{'X','O','X'},{'X','-','-'},{'-','-','-'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinXR2_output_true(){
        char [][]table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinOR2_output_true(){
        char [][]table = {{'-','-','-'},{'O','O','O'},{'-','-','-'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinOR2_output_false(){
        char [][]table = {{'O','X','-'},{'X','O','O'},{'-','-','-'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinXR3_output_true(){
        char [][]table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinOR3_output_true(){
        char [][]table = {{'-','-','-'},{'-','-','-'},{'O','O','O'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinOR3_output_False(){
        char [][]table = {{'-','-','-'},{'-','-','-'},{'-','O','O'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinOC1_output_true(){
        char [][]table = {{'O','-','-'},{'O','-','-'},{'O','-','-'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinXC1_output_true(){
        char [][]table = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinXC1_output_false(){
        char [][]table = {{'X','-','-'},{'O','O','-'},{'X','-','-'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinOC2_output_true(){
        char [][]table = {{'-','O','-'},{'-','O','-'},{'-','O','-'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinXC2_output_true(){
        char [][]table = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinXC2_output_false(){
        char [][]table = {{'O','X','-'},{'O','X','-'},{'O','O','-'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinOC3_output_true(){
        char [][]table = {{'-','-','O'},{'-','-','O'},{'-','-','O'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWinXC3_output_true(){
        char [][]table = {{'-','-','X'},{'-','-','X'},{'-','-','X'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Col_3_output_false(){
        char [][]table = {{'-','-','-'},{'-','-','O'},{'-','-','O'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckWinXcrossL_output_true(){
        char [][]table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_Cross_Left(table,Player);
        assertEquals(true, result);
    }
        
    @Test
    public void testCheckWinOcrossL_output_false(){
        char [][]table = {{'-','-','-'},{'-','O','-'},{'-','-','O'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_Cross_Left(table,Player);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWinXcrossR_output_true(){
        char [][]table = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        char Player = 'X';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinOcrossR_output_false(){
        char [][]table = {{'-','-','O'},{'-','O','-'},{'-','-','-'}};
        char Player = 'O';
        boolean result = Lab03.checkWin_RowCol(table,Player);
        assertEquals(false, result);
    }
    
    @Test
    public void testCheckDrawX_output_true(){
        char [][]table = {{'O','X','X'},{'X','O','O'},{'O','X','X'}};
        char Player = 'X';
        boolean result = Lab03.checkDraw(table,Player);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckDrawO_output_true(){
        char [][]table = {{'X','O','X'},{'X','O','O'},{'O','X','X'}};
        char Player = 'O';
        boolean result = Lab03.checkDraw(table,Player);
        assertEquals(true, result);
    }
    
}
